QSystem is a quantum computer simulator for Python. 

You can get QSystem using pip.
```bash
pip install qsystem
```
>>>
QSystem depends on [Boost C++ Libraries](https://www.boost.org) and requires
a C/C++ compiler.
>>>

For code examples access the [wiki](https://gitlab.com/evandro-crr/qsystem/wikis).

The source code is available at [Gitlab](https://gitlab.com/evandro-crr/qsystem).

